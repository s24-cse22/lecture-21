#include <igloo/igloo.h>
#include "analysis.h"

using namespace igloo;


Context(TestProgram) {
    Spec(IsMemberTest1){
        Assert::That(isMember(1), IsFalse());
    }
    Spec(IsMemberTest2){
        Assert::That(isMember(2), IsTrue());
    }
    Spec(IsMemberTest3){
        Assert::That(isMember(1984133), IsFalse());
    }
    Spec(IsMemberTest4){
        Assert::That(isMember(5254414), IsTrue());
    }
    
    Spec(MemberCountTest1){
        int list[10] = {1,2,3,4,5,6,7,8,9,10};
        Assert::That(memberCount(list, 10), Equals(5));
    }

    Spec(MemberCountTest2){
        int list[10] = {10,20,30,40,50,60,70,80,90,100};
        Assert::That(memberCount(list, 10), Equals(10));
    }
    Spec(MemberCountTest3){
        int list[10] = {11,21,31,41,51,61,71,81,91,101};
        Assert::That(memberCount(list, 10), Equals(0));
    }
    Spec(MemberCountTest4){
        int list[10] = {11,21,31,41,51,60,71,81,91,101};
        Assert::That(memberCount(list, 10), Equals(1));
    }

    Spec(NonMemberCountTest1){
        int list[10] = {1,2,3,4,5,6,7,8,9,10};
        Assert::That(nonMemberCount(list, 10), Equals(5));
    }

    Spec(NonMemberCountTest2){
        int list[10] = {10,20,30,40,50,60,70,80,90,100};
        Assert::That(nonMemberCount(list, 10), Equals(0));
    }
    Spec(NonMemberCountTest3){
        int list[10] = {11,21,31,41,51,61,71,81,91,101};
        Assert::That(nonMemberCount(list, 10), Equals(10));
    }
    Spec(NonMemberCountTest4){
        int list[10] = {11,21,31,41,51,60,71,81,91,101};
        Assert::That(nonMemberCount(list, 10), Equals(9));
    }

    Spec(DiamondCountTest1){
        int list[10] = {1,2,3,4,5,6,7,8,9,10};
        Assert::That(diamondCount(list, 10), Equals(0));
    }
    Spec(DiamondCountTest2){
        int list[10] = {1,2,3,4,5,6,7,8,9000000,10000000};
        Assert::That(diamondCount(list, 10), Equals(2));
    }
    Spec(DiamondCountTest3){
        int list[10] = {1,8888888,3,4,5,6,7,8,9000000,10000000};
        Assert::That(diamondCount(list, 10), Equals(3));
    }

    Spec(GoldCountTest1){
        int list[10] = {1,2,3,4,5,6,7,8,9,10};
        Assert::That(goldCount(list, 10), Equals(0));
    }
    Spec(GoldCountTest2){
        int list[10] = {1,2,3,4,5,6,7,8,6000000,7000000};
        Assert::That(goldCount(list, 10), Equals(2));
    }
    Spec(GoldCountTest3){
        int list[10] = {1,5888888,3,4,5,6,7,8,6000000,7000000};
        Assert::That(goldCount(list, 10), Equals(3));
    }

    Spec(SilverCountTest1){
        int list[10] = {1,2,3,4,5,6,7,8,9,10};
        Assert::That(silverCount(list, 10), Equals(5));
    }
    Spec(SilverCountTest2){
        int list[10] = {1,2,3,4,5,6,7,8,2000000,2000000};
        Assert::That(silverCount(list, 10), Equals(6));
    }
    Spec(SilverCountTest3){
        int list[10] = {1,8888888,3,4,5,6,7,8,7000000,3000000};
        Assert::That(silverCount(list, 10), Equals(4));
    }

    Spec(MembershipDuesTest1){
        Assert::That(membershipDues(0, 0, 0), Equals(0));
    }
    Spec(MembershipDuesTest2){
        Assert::That(membershipDues(67, 423, 8343), Equals(6956500));
    }
    Spec(MembershipDuesTest3){
        Assert::That(membershipDues(5, 34, 511), Equals(475500));
    }
};

int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}