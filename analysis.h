#ifndef ANALYSIS_H
#define ANALYSIS_H

bool isMember(int id){
    if (id % 2 == 0){
        return true;
    }
    else {
        return false;
    }
}

int memberCount(int list[], int size){
    int counter = 0;
    for (int i = 0; i < size; i++){
        if (isMember(list[i])){
            counter++;
        }
    }
    return counter;
}

int nonMemberCount(int list[], int size){
    int counter = 0;
    for (int i = 0; i < size; i++){
        if (!isMember(list[i])){
            counter++;
        }
    }
    return counter;
}

int silverCount(int list[], int size){
    int counter = 0;

    for (int i = 0; i < size; i++){
        if (isMember(list[i]) && list[i] < 4000000){
            counter++;
        }
    }

    return counter;
}

int goldCount(int list[], int size){
    int counter = 0;

    for (int i = 0; i < size; i++){
        if (isMember(list[i]) && list[i] >= 4000000 && list[i] < 8000000){
            counter++;
        }
    }

    return counter;
}

int diamondCount(int list[], int size){
    int counter = 0;

    for (int i = 0; i < size; i++){
        if (isMember(list[i]) && list[i] >= 8000000){
            counter++;
        }
    }

    return counter;
}

int membershipDues(int d, int g, int s){
    return (d * 10000) + (g * 5000) + (s * 500);
}

#endif